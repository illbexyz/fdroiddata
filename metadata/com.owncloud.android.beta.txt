Categories:Internet
License:GPLv2
Web Site:https://owncloud.org
Source Code:https://github.com/owncloud/android/tree/beta
Issue Tracker:https://github.com/owncloud/android/issues
Changelog:https://github.com/owncloud/android/blob/beta/CHANGELOG.md
Donate:https://www.bountysource.com/teams/owncloud

Auto Name:ownCloud beta
Summary:Synchronization client
Description:
ownCloud beta is a beta version of the official ownCloud app and includes
brand-new, untested features which might lead to instabilities and data loss.
The app is designed for users willing to test the new features and to report
bugs if they occur. Do not use it for your productive work!

The beta can be installed alongside the official ownCloud app which is available
at F-Droid, too.
.

Repo Type:git
Repo:https://github.com/owncloud/android.git

Build:ownCloud beta,20151125
    commit=beta-20151125
    submodules=yes
    gradle=yes
    srclibs=TouchImageView@v1.2.0
    rm=libs/disklrucache*,libs/android-support*,owncloud-android-library/libs,user_manual

Build:ownCloud beta,20151128
    commit=beta-20151128
    submodules=yes
    gradle=yes
    srclibs=TouchImageView@v1.2.0
    rm=libs/disklrucache*,libs/android-support*,owncloud-android-library/libs,user_manual

Build:20151129,20151129
    commit=beta-20151129
    submodules=yes
    gradle=yes
    srclibs=TouchImageView@v1.2.0
    rm=libs/disklrucache*,libs/android-support*,owncloud-android-library/libs,user_manual

Build:20151130,20151130
    commit=beta-20151130
    submodules=yes
    gradle=yes
    srclibs=TouchImageView@v1.2.0
    rm=libs/disklrucache*,libs/android-support*,owncloud-android-library/libs,user_manual

Build:20151202,20151202
    commit=beta-20151202
    submodules=yes
    gradle=yes
    srclibs=TouchImageView@v1.2.0
    rm=libs/disklrucache*,libs/android-support*,owncloud-android-library/libs,user_manual

Auto Update Mode:Version beta-%c
Update Check Mode:Tags
Current Version:20151202
Current Version Code:20151202
